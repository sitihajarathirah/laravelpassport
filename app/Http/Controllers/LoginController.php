<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;
use App\Models\User;

class LoginController extends Controller
{
    public function login()
    {
        return view('login');
    }
    //login
    public function loginApi(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:8',
        ]);

        try{
            $http = new \GuzzleHttp\Client;

            $email = $request->email;
            $password = $request->password;

            $user = auth()->user();
    
            $response = $http->post('http://localhost/laravelpassport/public/api/login?',[
                'headers' =>[
                    'Authorization' =>'Bearer'.session()->get('token.access_token')
                ],
                'query'=>[
                    'email'=>$email,
                    'password'=>$password
                ]
            ]);
    
            $result = json_decode((string)$response->getBody(),true);
    
            // return dd($result);
    
            return redirect()->route('home', compact('user'));

        }catch(\Exception $e){
            return redirect()->back()->with('error', 'Login fail, Please try again.');
        }
        
    }
}
